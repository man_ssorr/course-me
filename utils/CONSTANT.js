import { UserPlus, Login, InfoCircle } from 'tabler-icons-react';

export const HEADER_HEIGHT = 60;

export const links = [
  {
    url: '/signin',
    label: 'Sign In',
    icon: <Login size={20} strokeWidth={2} color={'#406fbf'} />,
  },
  {
    url: '/signup',
    label: 'Sign Up',
    icon: <UserPlus size={20} strokeWidth={2} color={'#406fbf'} />,
  },
  {
    url: '/about',
    label: 'About Us',
    icon: <InfoCircle size={20} strokeWidth={2} color={'#406fbf'} />,
  },
];
