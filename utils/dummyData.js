export const courses = [
  {
    id: 'C1',
    name: 'React',
    instructor: 'John Doe',
    description:
      'React is a JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers',
    image: 'https://source.unsplash.com/random/350x200',
    rating: 4.5,
    price: '$66',
  },
  {
    id: 'C2',
    name: 'Design',
    instructor: 'John Doe',
    description:
      'React is a JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers ',
    image: 'https://source.unsplash.com/random/350x200',
    rating: 3,
    price: '$37',
  },
  {
    id: 'C3',
    name: 'Python',
    instructor: 'John Doe',
    description:
      'React is a JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers ',
    image: 'https://source.unsplash.com/random/350x200',
    rating: 2.5,
    price: '$20',
  },
  {
    id: 'C4',
    name: 'Graphic Design',
    instructor: 'John Doe',
    description:
      'React is a JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers ',
    image: 'https://source.unsplash.com/random/350x200',
    rating: 5,
    price: '$80',
  },
  {
    id: 'C5',
    name: 'Graphic Design',
    instructor: 'John Doe',
    description:
      'React is a JavaScript library for building user interfaces. It is maintained by Facebook and a community of individual developers ',
    image: 'https://source.unsplash.com/random/350x200',
    rating: 5,
    price: '$80',
  },
];
