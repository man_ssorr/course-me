import { useForm } from '@mantine/form';
import {
  PasswordInput,
  TextInput,
  Button,
  Box,
  Group,
  Divider,
  RadioGroup,
  Radio,
  Title,
} from '@mantine/core';
import validator from 'validator';
import { Redirect } from 'react-router-dom';
// Icons
import { At, Lock } from 'tabler-icons-react';
// Buttons
import { GoogleButton, GithubButton, LinkedInButton } from '../SocialButtons';
import { useState } from 'react';
import { CheckIcon } from '@primer/octicons-react';

function SigninForm() {
  const [redirect, setRedirect] = useState(false);

  const form = useForm({
    initialValues: {
      email: '',
      password: '',
    },
    validate: {
      email: (value) => {
        return validator.isEmail(value) ? null : 'Enter a valid email';
      },
    },
  });

  const onSubmitHanddler = async (values) => {
    try {
      // const response = await axios.post("/users/login", {
      //   email: values.email,
      //   password: values.password,
      // });

      localStorage.setItem('userMe', JSON.stringify(response.data.data));
    } catch (error) {
      console.log(error.message);
    }
  };

  // if (redirect) {
  //   return <Redirect to="/" />;
  // }

  return (
    <Box
      sx={{
        maxWidth: 550,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        margin: 'auto',
        border: '1px solid #eaeaea',
        borderRadius: '14px',
        paddingTop: '20px',
        boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)',
      }}
    >
      <form onSubmit={form.onSubmit(onSubmitHanddler)}>
        <Title
          order={2}
          sx={(theme) => ({
            color: theme.colors.blue[4],
          })}
        >
          Login
        </Title>

        <TextInput
          icon={<At />}
          // required
          placeholder="mansour@mail.com"
          radius="xs"
          mt="sm"
          size="md"
          {...form.getInputProps('email')}
        />

        <PasswordInput
          // required
          placeholder="Your password"
          icon={<Lock size={16} />}
          radius="xs"
          mt="sm"
          size="md"
          {...form.getInputProps('password')}
        />

        <Group position="center" mt="xl">
          <Button type="submit" size="md" fullWidth radius={5}>
            Login
          </Button>
        </Group>
      </form>

      <Divider label="Or continue with email" labelPosition="center" my="lg" />
    </Box>
  );
}

export default SigninForm;
