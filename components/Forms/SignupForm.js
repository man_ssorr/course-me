import { useForm } from '@mantine/form';
import {
  PasswordInput,
  TextInput,
  Button,
  Box,
  Group,
  Divider,
  RadioGroup,
  Radio,
  Title,
} from '@mantine/core';
import validator from 'validator';
import { Redirect } from 'react-router-dom';
// Icons
import { At, Lock } from 'tabler-icons-react';
// Buttons
import { GoogleButton, GithubButton, LinkedInButton } from '../SocialButtons';
import { useState } from 'react';
import { CheckIcon } from '@primer/octicons-react';

function SignupForm() {
  const [redirect, setRedirect] = useState(false);

  const form = useForm({
    initialValues: {
      fname: '',
      lname: '',
      email: '',
      password: '',
      confirmPassword: '',
      rule: 'Student',
    },
    validate: {
      fname: (value, values) => {
        return value.length < 2
          ? 'Name must have at least 2 letters'
          : null || value === values.lname
          ? "Your first name can't be your last name!"
          : null;
      },
      lname: (value, values) => {
        return value.length < 2
          ? 'Name must have at least 2 letters'
          : null || value === values.fname
          ? 'Same error!'
          : null;
      },
      email: (value) => {
        return validator.isEmail(value) ? null : 'Enter a valid email';
      },

      password: (value, values) => {
        return value.length < 8
          ? 'Your password should be more than 8 in length'
          : null || validator.contains(value, values.fname, { ignoreCase: true })
          ? "Your name can't be password!"
          : null || validator.contains(value, values.lname, { ignoreCase: true })
          ? "Your last name can't be password!"
          : null || validator.contains(value, values.email.split('@')[0], { ignoreCase: true })
          ? "Your email can't be password!"
          : null;
      },
      confirmPassword: (value, values) => {
        return value !== values.password ? 'Passwords did not match' : null;
      },
    },
  });

  const onSubmitHanddler = async (values) => {
    try {
      // const response = await axios.post('/users/add', {
      //   firstName: values.fname,
      //   lastName: values.lname,
      //   email: values.email,
      //   password: values.password,
      //   rule: values.rule,
      // });

      localStorage.setItem('userMe', JSON.stringify(response.data.data));
    } catch (error) {
      console.log(error.message);
    }
  };

  // if (redirect) {
  //   return <Redirect to="/login" />;
  // }

  return (
    <Box
      sx={{
        maxWidth: 550,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        margin: 'auto',
        border: '1px solid #eaeaea',
        borderRadius: '14px',
        paddingTop: '20px',
        boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)',
      }}
    >
      <form onSubmit={form.onSubmit(onSubmitHanddler)}>
        <Title
          order={2}
          sx={(theme) => ({
            color: theme.colors.blue[4],
          })}
        >
          Register
        </Title>

        <RadioGroup
          value={form.values.rule}
          onChange={(value) => form.setFieldValue('rule', value)}
          label="Choose: How you wanna use courseme"
          description="You can change this in settings."
          spacing="xl"
          size="md"
          required
          sx={(theme) => ({
            display: 'flex',
            flexFlow: 'column nowrap',
            justifyContent: 'space-between',
          })}
        >
          <Radio mr={135} value="Student" label="Student" />
          <Radio size="md" value="Instructor" label="Instructor" />
        </RadioGroup>
        <Group position="center" grow>
          <TextInput
            // required
            placeholder="First Name"
            mt="sm"
            size="md"
            {...form.getInputProps('fname')}
          />

          <TextInput
            // required
            placeholder="Last Name"
            mt="sm"
            size="md"
            {...form.getInputProps('lname')}
          />
        </Group>

        <TextInput
          icon={<At />}
          // required
          placeholder="mansour@mail.com"
          radius="xs"
          mt="sm"
          size="md"
          {...form.getInputProps('email')}
        />

        <PasswordInput
          // required
          placeholder="Your password"
          icon={<Lock size={16} />}
          radius="xs"
          mt="sm"
          size="md"
          {...form.getInputProps('password')}
        />

        <PasswordInput
          // required
          placeholder="Confirm password"
          icon={<Lock size={16} />}
          radius="xs"
          mt="sm"
          size="md"
          {...form.getInputProps('confirmPassword')}
        />

        <Group position="center" mt="xl">
          <Button type="submit" size="md" fullWidth radius={5}>
            Register
          </Button>
        </Group>
      </form>

      <Divider label="Or continue with email" labelPosition="center" my="lg" />
    </Box>
  );
}

export default SignupForm;
