import React from 'react';
import Courses from './Courses';
import { Card, Image, Text, Badge, Button, Group, useMantineTheme } from '@mantine/core';
import { Star } from 'tabler-icons-react';
import useStyles from './styles';

const CourseCard = (course) => {
  const { classes } = useStyles();
  return (
    <div style={{ width: 340, margin: 'auto' }}>
      <Card p="lg" m="xs" className={classes.Card}>
        <Card.Section>
          <Image src={course.image} height={160} alt={CourseCard.title} />
        </Card.Section>

        <Group position="apart" style={{ marginBottom: 5, marginTop: 5 }}>
          <Text weight={500}>{course.name}</Text>
          <Badge color="blue" variant="light">
            {course.price}
          </Badge>
        </Group>

        <Group position="apart" style={{ marginBottom: 5, marginTop: 5 }}>
          <Text weight={300}>{course.instructor}</Text>
          <Badge color="yellow" variant="light" icon={<Star />}>
            Rating : {course.rating}
          </Badge>
        </Group>

        <Text size="sm" style={{ lineHeight: 1.5 }}>
          {course.description}
        </Text>
      </Card>
    </div>
  );
};

export default CourseCard;
