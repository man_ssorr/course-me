import { createStyles } from '@mantine/core';

const useStyles = createStyles((theme) => ({
  Container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },

  Card: {
    width: '90%',
    margin: 'auto',
    borderRadius: theme.radius.md,
    boxShadow: theme.shadows.sm,
    '&:hover': {
      boxShadow: '(0px 0px 10px rgba(0, 0, 0, 0.1))',
    },
  },
}));

export default useStyles;
