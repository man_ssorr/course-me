import React from 'react';
import { Container, Grid, Text } from '@mantine/core';
import CourseCard from './CourseCard.js';
import useStyles from './styles.js';
import { courses } from '../../utils/dummyData.js';

const Courses = () => {
  const { classes } = useStyles();
  return (
    <Container dir="row" size="xl">
      <div className={classes.Container}>
        <Grid>
          {courses.map((course) => (
            <Grid.Col sm={12} md={6} lg={3}>
              <CourseCard className={classes.CourseCard} key={course.id} {...course} />
            </Grid.Col>
          ))}
        </Grid>
      </div>
    </Container>
  );
};

export default Courses;
