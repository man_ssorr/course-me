// CONSTANT AND METHODS
import { HEADER_HEIGHT } from '../../utils/CONSTANT';
// REACT
// NEXT
import Link from 'next/link';
// REDUX
// MANTINE
import {
  Menu,
  Center,
  Header,
  Container,
  Group,
  Button,
  Burger,
  Text,
  Collapse,
} from '@mantine/core';
import { useBooleanToggle } from '@mantine/hooks';
// COMPONENTS
import { ColorSchemeToggle } from '../ColorSchemeToggle/ColorSchemeToggle';
// STYLES
import useStyles from './styles';
// PACKAGES
import { ChevronDown } from 'tabler-icons-react';

export default function TopNav({ links }) {
  const { classes } = useStyles();
  const [opened, toggleOpened] = useBooleanToggle(false);

  return (
    <Header height={HEADER_HEIGHT} sx={{ borderBottom: 0 }} mb={40}>
      <Container className={classes.inner} fluid>
        <Link href="/" className={classes.link}>
          <Text className={classes.logoText}>Courseme</Text>
        </Link>

        <Group>
          <Burger
            opened={opened}
            onClick={() => toggleOpened()}
            className={classes.burger}
            size="sm"
          />
          <Collapse in={opened} className={classes.collapse}>
            {links.map((link) => (
              <Link href={link.url} key={link.label}>
                <a className={classes.link}>
                  {link.icon}
                  {link.label}
                </a>
              </Link>
            ))}
          </Collapse>
        </Group>

        <Group spacing={5} className={classes.links}>
          {links.map((link) => (
            <Link href={link.url} key={link.label}>
              <a className={classes.link}>
                {link.icon}
                {link.label}
              </a>
            </Link>
          ))}
          <Button radius="xl" sx={{ height: 36 }}>
            Become an instructor
          </Button>
        </Group>
      </Container>
    </Header>
  );
}

// CONSTANT AND METHODS
// REACT
// NEXT
// REDUX
// MANTINE
// COMPONENTS
// STYLES
// PACKAGES
