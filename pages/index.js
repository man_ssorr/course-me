// import { Welcome } from '../components/Welcome/Welcome';
import Courses from '../components/Courses/';

import { ColorSchemeToggle } from '../components/ColorSchemeToggle/ColorSchemeToggle';

export default function HomePage() {
  return (
    <>
      <Courses />
      <ColorSchemeToggle />
    </>
  );
}
