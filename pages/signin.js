// CONSTANT AND METHODS
// REACT
import { useState, useContext, useEffect } from 'react';
// NEXT
import Head from 'next/head';
import Link from 'next/link';
// REDUX
// MANTINE
import { Text } from '@mantine/core';
import { Notification } from '@mantine/core';
// COMPONENTS
// STYLES
// PACKAGES
import axios from 'axios';
import { SigninForm } from '../components/Forms';

function signin() {
  return (
    <>
      <Head>
        <title>Signin - Course me</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        <link rel="shortcut icon" href="/favicon.svg" />
      </Head>
      <SigninForm />
    </>
  );
}

export default signin;
