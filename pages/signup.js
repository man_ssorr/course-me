import { Text } from '@mantine/core';
import Head from 'next/head';
import { SignupForm } from '../components/Forms';
function signup() {
  return (
    <>
      <Head>
        <title>Signup - Course me</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        <link rel="shortcut icon" href="/favicon.svg" />
      </Head>

      <SignupForm />
    </>
  );
}

export default signup;
